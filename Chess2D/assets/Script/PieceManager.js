const CellColor = require("./Enum/CellColor");
const CellState = require("./Enum/CellState");
const Cell = require("./Cell/Cell");
const PieceJSPath = require("./PieceJSPath");

cc.Class({
    extends: cc.Component,
    properties: {
        pieceFrefab: cc.Prefab,
        cell: cc.Prefab,
        nodeBoard: cc.Node,
        noticeChess: cc.Node,

        boardChessPiece: { default: null, visible: false },
        listWhitePieces: [],
        listBlackPieces: [],
        listNamePieces: [],
        boardCell: [],
    },


    onLoad: function () {

        this.listNamePieces = ["PAWN", "PAWN", "PAWN", "PAWN", "PAWN", "PAWN", "PAWN", "PAWN",
            "ROOK", "KNIGHT", "BISHOP", "QUEEN", "KING", "BISHOP", "KNIGHT", "ROOK"];

        this.createBoardCell();
        this.createBoardChessPiece();
    },

    createBoardCell: function () {
        let arrCell = [];
        for (let i = 0; i < 8; i++) {
            arrCell = [];
            for (let j = 0; j < 8; j++) {
                let cellTemp = cc.instantiate(this.cell);
                let posCell = this.getPosition(cellTemp, i, j);

                (i + j) % 2 == 0 ?
                    cellTemp.getComponent(Cell).setup(new cc.v2(i, j), CellColor.BLACK, this) :
                    cellTemp.getComponent(Cell).setup(new cc.v2(i, j), CellColor.WHITE, this);

                cellTemp.setPosition(posCell);
                this.nodeBoard.addChild(cellTemp);
                arrCell.push(cellTemp.getComponent(Cell));
            }
            this.boardCell.push(arrCell);
        }
    },

    ValidateCell: function (targetX, targetY, chessPiece) {
        if (targetX < 0 || targetX > 7) {
            return CellState.OutOfBounds;
        }
        if (targetY < 0 || targetY > 7) {
            return CellState.OutOfBounds;
        }

        let targetCell = this.boardCell[targetX][targetY];
        if (targetCell.currentPiece) {
            if (chessPiece.colorChess == targetCell.currentPiece.colorChess) {
                return CellState.Friendly;
            }
            if (chessPiece.colorChess != targetCell.currentPiece.colorChess) {
                return CellState.Enemy;
            }
        }
        return CellState.Free;
    },

    createBoardChessPiece: function () {

        this.listBlackPieces = this.createPieces(CellColor.BLACK);
        this.listWhitePieces = this.createPieces(CellColor.WHITE);

        this.placePieces(1, 0, this.listBlackPieces);
        this.placePieces(6, 7, this.listWhitePieces);
    },

    getPosition: function (pf, i, j) {
        return cc.v2(j * pf.width, i * pf.height);
    },

    createPieces: function (color) {
        let arrPiece = [];
        for (let i = 0; i < this.listNamePieces.length; i++) {

            let nameRequire = PieceJSPath(this.listNamePieces[i]);
            let chessPiece = cc.instantiate(this.pieceFrefab);
            chessPiece.addComponent(nameRequire);
            chessPiece.getComponent(nameRequire).setup(this.listNamePieces[i], color, this);
            this.node.addChild(chessPiece);

            arrPiece.push(chessPiece.getComponent(nameRequire));
        }

        return arrPiece;
    },

    placePieces: function (pawnRow, royalRow, listPiece) {
        for (let i = 0; i < 8; i++) {
            listPiece[i].place(this.boardCell[pawnRow][i]);
            listPiece[i + 8].place(this.boardCell[royalRow][i]);
        }
    },

    setInteractive: function (listPiece, enable) {
        listPiece.forEach(element => {
            element.node.getComponent(cc.Button).interactable = enable;
        });
    },

    switchSides: function (color) {
        let isBlackTurn = color == CellColor.BLACK ? true : false;

        this.setInteractive(this.listWhitePieces, isBlackTurn);
        this.setInteractive(this.listBlackPieces, !isBlackTurn);

        if (isBlackTurn) {
            this.computerMove();
        }
    },

    computerMove: function () {
        let pieceRandom = null;
        let lighHight = [];

        while (pieceRandom == null) {
            let index = Math.floor(Math.random() * (this.listWhitePieces.length)) + 1;
            lighHight = this.listWhitePieces[index - 1].hasMove();

            if (lighHight.length) {
                pieceRandom = this.listWhitePieces[index - 1];
            }
        }

        if (pieceRandom) {
            setTimeout(() => {
                pieceRandom.moveComputer();
            }, 1100);
        } else return;

    },

    showNotice: function () {
        this.noticeChess.active = true;
    },

    hideNotice: function () {
        this.noticeChess.active = false;
        cc.director.loadScene("GameChess");
    }
});
