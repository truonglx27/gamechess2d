const ChessPieceName = cc.Enum({
    KING: "KING",
    QUEEN: "QUEEN",
    KNIGHT: "KNIGHT",
    ROOK: "ROOK",
    PAWN: "PAWN",
    BISHOP: "BISHOP",
})

module.exports = ChessPieceName;