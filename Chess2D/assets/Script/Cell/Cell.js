const CellColor = require("./../Enum/CellColor");
cc.Class({
    extends: cc.Component,
    properties: {
        posCell: { default: null, type: cc.v2, visible: false },
        board: { default: null, visible: false },
        currentPiece: null,
    },

    setColor: function (color) {
        switch (color) {
            case CellColor.BLACK:
                this.node.color = cc.Color(186, 177, 150, 255);
                break;
            case CellColor.WHITE:
                this.node.color = cc.Color(230, 220, 186, 255);
                break;
        }
    },

    setActiveHover: function (isActive) {
        this.node.getChildren()[0].active = isActive;
    },

    setup: function (posCell, color, newBoard) {
        this.posCell = posCell;
        this.setColor(color);
        this.board = newBoard;
    },

    removePiece: function () {
        if (this.currentPiece) {
            this.currentPiece.kill();
        }
    }
});