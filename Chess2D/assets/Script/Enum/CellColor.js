const CellColor = cc.Enum({
    WHITE: "WHITE",
    BLACK: "BLACK"
});

module.exports = CellColor;