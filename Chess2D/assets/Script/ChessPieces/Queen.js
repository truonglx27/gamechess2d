var BaseChessPiece = require('../BaseChess');
cc.Class({
    extends: BaseChessPiece,
    properties: {
    },
    onLoad: function () {
    },
    setup: function (pieceName, color, currentPiece) {
        this._super(pieceName, color, currentPiece);
        this.moveMent = new cc.v3(7, 7, 7);
    },
});