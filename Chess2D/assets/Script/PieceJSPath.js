const PieceName = require('./Enum/ChessPieceName');
const PieceJSPath = function (pieceName) {
    switch (pieceName) {
        case PieceName.PAWN:
            return require("./ChessPieces/Pawn");
        case PieceName.KING:
            return require("./ChessPieces/King");
        case PieceName.QUEEN:
            return require("./ChessPieces/Queen");
        case PieceName.BISHOP:
            return require("./ChessPieces/Bishop");
        case PieceName.KNIGHT:
            return require("./ChessPieces/Knight");
        case PieceName.ROOK:
            return require("./ChessPieces/Rook");
    }
}

module.exports = PieceJSPath;