const CellState = cc.Enum({
    None: "NONE",
    Friendly: "FRIENDLY",
    Enemy: "Enemy",
    Free: "FREE",
    OutOfBounds: "OUTOFBOUND",
});

module.exports = CellState;