var PIECENAME = require('./Enum/ChessPieceName');
var CellColor = require("./Enum/CellColor");
var Cell = require('./Cell/Cell');
var CellState = require('./Enum/CellState');
const { Free } = require('./Enum/CellState');
cc.Class({
    extends: cc.Component,
    properties: {
        colorChess: { default: null, type: CellColor, visible: false },
        currentCell: { default: null, type: Cell, visible: false },
        targetCell: { default: null, visible: false },
        nameChess: { default: null, type: PIECENAME, visible: false },
        chessPiece: { default: null, visible: false },
        hightLightedCells: [],
        moveMent: { default: null, type: cc.v3, visible: false },
        piceManager: { default: null, type: CellColor, visible: false },
    },

    start: function () {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onDrag, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
    },

    setImgPiece: function (pieceName) {
        this.nameChess = pieceName;
        this.chessPiece = this.node.getChildren()[0];
        let instance = this;
        cc.loader.loadRes('ChessPiece1', cc.SpriteAtlas, function (err, atlas) {
            atlas.getSpriteFrame(pieceName);
            instance.chessPiece.getComponent(cc.Sprite).spriteFrame = atlas.getSpriteFrame(pieceName);
        });
    },

    changeColor: function (color) {
        this.colorChess = color;
        if (color == CellColor.WHITE) {
            this.chessPiece.color = cc.Color(210, 94, 63, 255);
        } else {
            this.chessPiece.color = cc.Color(71, 64, 64, 255);
        }
    },

    setup: function (pieceName, color, newPieceManager) {
        this.setImgPiece(pieceName);
        this.changeColor(color);
        this.piceManager = newPieceManager;
    },

    place: function (newCell) {
        this.currentCell = newCell;
        this.currentCell.currentPiece = this;
        this.node.setPosition(newCell.node.getPosition());
    },

    createCellPath: function (xDirection, yDirection, moveMent) {
        let currentX = this.currentCell.posCell.x;
        let currentY = this.currentCell.posCell.y;

        for (let i = 1; i <= moveMent; i++) {
            currentX += yDirection;
            currentY += xDirection;

            // check cell currentX, currentY 
            let cellState = CellState.None;
            cellState = this.currentCell.board.ValidateCell(currentX, currentY, this);

            if (cellState == CellState.Enemy) {
                this.hightLightedCells.push(this.currentCell.board.boardCell[currentX][currentY]);
                break;
            }
            if (cellState != CellState.Free) {
                break;
            }
            this.hightLightedCells.push(this.currentCell.board.boardCell[currentX][currentY]);
        }
    },

    kill: function () {
        this.currentCell.currentPiece = null;
        this.node.active = false;
    },


    checkPathing: function () {
        // horizontal
        this.createCellPath(-1, 0, this.moveMent.x);
        this.createCellPath(1, 0, this.moveMent.x);

        // vertical 
        this.createCellPath(0, 1, this.moveMent.y);
        this.createCellPath(0, -1, this.moveMent.y);

        // Upper diagonal
        this.createCellPath(1, 1, this.moveMent.z);
        this.createCellPath(-1, 1, this.moveMent.z);

        //Lower diagonal
        this.createCellPath(1, -1, this.moveMent.z);
        this.createCellPath(-1, -1, this.moveMent.z);
    },

    showHighlightCell: function () {
        this.hightLightedCells.forEach(cell => {
            cell.setActiveHover(true);
        });
    },

    clearHighlightCell: function () {
        this.hightLightedCells.forEach(cell => {
            cell.setActiveHover(false);
        });

        this.hightLightedCells = [];
    },

    move: function () {
        this.targetCell.removePiece();
        this.currentCell.currentPiece = null;
        this.currentCell = this.targetCell;
        this.currentCell.currentPiece = this;
        this.node.setPosition(this.targetCell.node.getPosition());
        this.targetCell = null;

        this.piceManager.switchSides(this.colorChess);
    },

    onDrag: function (event) {
        if (!this.getInteractive()) return;

        var delta = event.touch.getDelta();
        this.node.x += delta.x;
        this.node.y += delta.y;
    },

    onTouchStart: function () {
        if (!this.getInteractive()) return;

        this.pieceCurrent = this.node.getPosition();
        this.checkPathing();
        this.showHighlightCell();
    },

    onTouchEnd: function () {
        if (!this.getInteractive()) return;

        this.getTargetPieceMove();
        
        if (this.targetCell) {
            this.move();
        } else {
            this.node.setPosition(this.pieceCurrent);
        }
        this.clearHighlightCell();
    },

    getTargetPieceMove: function () {
        for (let i = 0; i < this.hightLightedCells.length; i++) {
            let cell = this.hightLightedCells[i];
            let xCell = cell.node.getPosition().x;
            let yCell = cell.node.getPosition().y;
            let widthCell = cell.node.width; // width = height 
            let pieceCurrent = this.node;

            if (
                pieceCurrent.x <= (xCell + widthCell / 2) && pieceCurrent.x >= (xCell - widthCell / 2) &&
                pieceCurrent.y <= (yCell + widthCell / 2) && pieceCurrent.y >= (yCell - widthCell / 2)
            ) {
                this.targetCell = cell;
                break;
            }
        }
    },

    getInteractive: function () {
        return this.node.getComponent(cc.Button).interactable;
    },

    hasMove: function () {
        this.checkPathing();
        if (!this.getActivePiece()) {
            return [];
        }
        else return this.hightLightedCells;
    },
    moveComputer: function () {
        let index = Math.floor(Math.random() * (this.hightLightedCells.length)) + 1;
        this.targetCell = this.hightLightedCells[index - 1];
        this.move();
        this.clearHighlightCell();
    },

    getActivePiece: function () {
        return this.node.active;
    }
});