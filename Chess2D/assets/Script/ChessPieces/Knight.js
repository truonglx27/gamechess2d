var BaseChessPiece = require('../BaseChess');
var CellState = require('./../Enum/CellState');

cc.Class({
    extends: BaseChessPiece,
    properties: {
    },
    onLoad: function () {
    },
    setup: function (pieceName, color, currentPiece) {
        this._super(pieceName, color, currentPiece);
        // this.moveMent = color == CellColor.BLACK ? new cc.v3(-1, 2, 0) : new cc.v3(1, 2, 0);
        // this.moveMent = new cc.v3(-1, 2, 0);
    },
    matchesState: function (targetX, targetY) {
        var cellCurrent = this.currentCell.board.ValidateCell(targetY, targetX, this);
        if (cellCurrent != CellState.Friendly && cellCurrent != CellState.OutOfBounds) {
            this.hightLightedCells.push(this.currentCell.board.boardCell[targetY][targetX]);
            return true;
        }
        return false;
    },
    checkPathing: function () {
        this.createPath(1);
        this.createPath(-1);

    },

    createPath: function (flipper) {
        let currentX = this.currentCell.posCell.y;
        let currentY = this.currentCell.posCell.x;

        this.matchesState(currentX - 1, currentY + (2 * flipper));

        this.matchesState(currentX - 2, currentY + (1 * flipper));

        this.matchesState(currentX + 1, currentY + (2 * flipper));

        this.matchesState(currentX + 2, currentY + (1 * flipper));
    }
});