var BaseChessPiece = require('../BaseChess');
var CellColor = require("./../Enum/CellColor");
var PIECENAME = require('./..//Enum/ChessPieceName');
var Cell = require('./../Cell/Cell');
var CellState = require('./../Enum/CellState');

cc.Class({
    extends: BaseChessPiece,
    properties: {
    },
    setup: function (pieceName, color, currentPiece) {
        this._super(pieceName, color, currentPiece);
        this.moveMent = color == CellColor.BLACK ? new cc.v3(0, 1, 1) : new cc.v3(0, -1, -1);
    },
    matchesState: function (targetX, targetY, cellState) {
        var cellCurrent = this.currentCell.board.ValidateCell(targetY, targetX, this);
        if (cellCurrent == cellState) {
            this.hightLightedCells.push(this.currentCell.board.boardCell[targetY][targetX]);
            return true;
        }
        return false;
    },
    checkPathing: function () {
        let currentX = this.currentCell.posCell.y;
        let currentY = this.currentCell.posCell.x;

        // Top Left
        this.matchesState(currentX - this.moveMent.z, currentY + this.moveMent.z, CellState.Enemy);

        this.matchesState(currentX, currentY + this.moveMent.y, CellState.Free);

        //Top Right
        this.matchesState(currentX + this.moveMent.z, currentY + this.moveMent.z, CellState.Enemy);

    }
});